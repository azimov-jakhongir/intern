package com.company.service;

import com.company.model.form.ProductForm;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface ProductService {
    ResponseEntity<?> createProduct(ProductForm productForm);

    ResponseEntity<?> findByProductId(Long productId);

    ResponseEntity<?> findAllProducts();
    ResponseEntity<?> updateByProductId(Long productId, ProductForm productForm);

    ResponseEntity<?> deleteByProductId(Long productId);
}
