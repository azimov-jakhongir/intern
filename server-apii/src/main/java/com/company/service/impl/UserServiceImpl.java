package com.company.service.impl;

import com.company.config.JwtService;
import com.company.model.dto.ResponseDto;
import com.company.model.entity.UserEntity;
import com.company.model.enums.Roles;
import com.company.model.form.UserForm;
import com.company.model.form.UserLoginForm;
import com.company.repository.UserRepository;
import com.company.service.UserService;
import com.company.service.base.Mapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;

    private final JwtService jwtService;

    private final Mapper mapper;

    @Override
    public ResponseEntity<?> doRegister(UserForm userForm) {
        ResponseDto<?> response;

        UserEntity user;
        try {
            user = userRepository.save(UserEntity.builder()
                    .firstName(userForm.getFirstName())
                    .lastName(userForm.getLastName())
                    .username(userForm.getUsername())
                    .password(passwordEncoder.encode(userForm.getPassword()))
                    .roles(Roles.USER)
                    .build());

        } catch (Exception e) {
            log.warn("User creation failed, Item found exception");
            response = mapper.convertResponseDto(null, "Item found exception", false, 494);
            return ResponseEntity.status(HttpStatusCode.valueOf(200)).body(response);
        }

        log.info("User saved successfully: {}", user);
        response = mapper.convertResponseDto(mapper.formToDto(userForm, user.getId()), "success", true, 200);
        return ResponseEntity.status(HttpStatusCode.valueOf(200)).body(response);

    }


    @Override
    public ResponseEntity<?> addUser(UserForm userForm) {
        ResponseDto<?> response;

        UserEntity user;
        try {
            user = userRepository.save(UserEntity.builder()
                    .firstName(userForm.getFirstName())
                    .lastName(userForm.getLastName())
                    .username(userForm.getUsername())
                    .password(passwordEncoder.encode(userForm.getPassword()))
                    .roles(userForm.getRole())
                    .build());

        } catch (Exception e) {
            log.warn("User creation failed, Item found exception");
            response = mapper.convertResponseDto(null, "Item found exception", false, 494);
            return ResponseEntity.status(HttpStatusCode.valueOf(200)).body(response);
        }

        log.info("User saved successfully: {}", user);
        response = mapper.convertResponseDto(mapper.formToDto(userForm, user.getId()), "success", true, 200);
        return ResponseEntity.status(HttpStatusCode.valueOf(200)).body(response);

    }

    @Override
    public ResponseEntity<?> doLogin(UserLoginForm userLoginForm) {
        String token = "";

        ResponseDto<?> response;

        Optional<UserEntity> username =
                userRepository.findByUsername(userLoginForm.getUsername());
        String dtoUsername = "";
        String role = "";

        if (username.isEmpty()) {
            log.warn("There is no user with this username user{}", userLoginForm);
            response = mapper.convertResponseDto(null, "Username or password wrong", false, 401);
            return ResponseEntity.status(HttpStatusCode.valueOf(401)).body(response);
        }

        if (!username.get().getState()) {
            log.warn("User was deleted user{}", userLoginForm);
            response = mapper.convertResponseDto(null, "User is deleted", false, 404);
            return ResponseEntity.status(HttpStatusCode.valueOf(404)).body(response);

        }
        Authentication auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userLoginForm.getUsername(), userLoginForm.getPassword()));


        if (!auth.isAuthenticated()) {
            log.warn("There is no user with this username user{}", userLoginForm);
            response = mapper.convertResponseDto(null, "Username or password wrong", false, 500);
            return ResponseEntity.status(HttpStatusCode.valueOf(500)).body(response);
        } else {
            token = jwtService.generateToken(userLoginForm.getUsername());
//            dtoUsername = username.get().getUsername();
//            role = username.get().getRoles().toString();
        }
        log.info("logged success");
//            Map<String, String> map = new HashMap<>();
//            map.put("token", token);
//            map.put("dtoUsername", dtoUsername);
//            map.put("role", role);

        response = mapper.convertResponseDto(token, "success", true, 200);
//            response = mapper.convertResponseDto(map, "success", true, 200);
        return ResponseEntity.status(HttpStatusCode.valueOf(200)).body(response);
    }
}
