package com.company.service.impl;

import com.company.model.dto.ResponseDto;
import com.company.model.entity.ProductEntity;
import com.company.model.form.ProductForm;
import com.company.repository.ProductRepository;
import com.company.service.ProductService;
import com.company.service.base.Mapper;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private Mapper mapper;

    @Override
    public ResponseEntity<?> createProduct(ProductForm productForm) {
        try {
            ProductEntity product;
            product = productRepository.save(
                    ProductEntity.builder()
                            .name(productForm.getName())
                            .color(productForm.getColor())
                            .price(productForm.getPrice())
                            .weight(productForm.getWeight())
                            .createdAt(LocalDateTime.now())
                            .build()
            );
            ResponseDto<?> successResponse = mapper.convertResponseDto(product, "Success", true, 200);
            return ResponseEntity.status(HttpStatus.OK).body(successResponse);
        } catch (Exception e) {
            log.warn("Product creation failed", e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Product creation failed", false, 500);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }

    }

    @Override
    public ResponseEntity<?> findByProductId(Long productId) {
        ResponseDto<?> response;

        ProductEntity product;
        try {
            product = productRepository.findById(productId)
                    .orElseThrow(() -> new EntityNotFoundException("Product not found with id: " + productId));
        } catch (EntityNotFoundException e) {
            log.warn("Product not found with id: " + productId, e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Product not found", false, 404);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        } catch (Exception e) {
            log.error("Error retrieving product with id: " + productId, e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Internal Server Error", false, 500);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }

        log.info("Product found: {}", product);
        response = mapper.convertResponseDto(product, "Success", true, 200);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @Override
    public ResponseEntity<?> findAllProducts() {
        ResponseDto<?> response;

        List<ProductEntity> productList;
        try {
            productList = productRepository.findAll();
        } catch (Exception e) {
            log.error("Error retrieving all products", e);
            response = mapper.convertResponseDto(null, "Internal Server Error", false, 500);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }

        response = mapper.convertResponseDto(productList, "Success", true, 200);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @Override
    public ResponseEntity<?> updateByProductId(Long productId, ProductForm productForm) {
        ResponseDto<?> response;

        ProductEntity existingProduct;
        ProductEntity updatedEntity;
        try {

            existingProduct = productRepository.findById(productId)
                    .orElseThrow(() -> new EntityNotFoundException("Product not found with id:" + productId));

            existingProduct.setName(productForm.getName());
            existingProduct.setColor(productForm.getColor());
            existingProduct.setWeight(productForm.getWeight());
            existingProduct.setPrice(productForm.getPrice());

            updatedEntity = productRepository.save(existingProduct);
        } catch (EntityNotFoundException e) {
            log.warn("Product not found with id: " + productId, e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Product not found", false, 404);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        } catch (Exception e) {
            log.warn("Error updating product with id: " + productId, e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Internal server error", false, 500);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }

        response = mapper.convertResponseDto(updatedEntity, "Product updated successfully", true, 200);
        return ResponseEntity.status(HttpStatus.OK).body(response);

    }

    @Override
    public ResponseEntity<?> deleteByProductId(Long productId) {
        ResponseDto<?> response;
        ProductEntity existingProduct;

        try {
            existingProduct = productRepository.findById(productId)
                    .orElseThrow(() -> new EntityNotFoundException("Product not dound with id" + productId));
            productRepository.delete(existingProduct);

        } catch (EntityNotFoundException e) {
            log.warn("Product not found with id: " + productId, e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Product not found", false, 404);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        } catch (Exception e) {
            log.warn("Error deleting product with id: " + productId, e);
            ResponseDto<?> errorResponse = mapper.convertResponseDto(null, "Interval server error", false, 500);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }

        response = mapper.convertResponseDto(null, "Product deleted successfully", true, 200);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
