package com.company.service.impl;

import com.company.model.dto.ResponseDto;
import com.company.model.entity.ProductEntity;
import com.company.repository.ProductRepository;
import com.company.service.FileService;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.hibernate.internal.util.collections.ArrayHelper.forEach;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private final ProductRepository productRepository;


    @Override
    public ResponseDto<String> exelCreate() throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet("Product Data");
        XSSFRow row;

        List<ProductEntity> productList = productRepository.findAll();
        int rowId = 0;
        for (ProductEntity product : productList) {
            row = spreadsheet.createRow(rowId++);
            row.createCell(0).setCellValue(product.getId());
            row.createCell(1).setCellValue(product.getName());
            row.createCell(2).setCellValue(product.getColor());
            row.createCell(3).setCellValue(product.getWeight());
            row.createCell(4).setCellValue(product.getPrice());
            row.createCell(5).setCellValue(product.getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }

        String filePath = generateFilePath("upload_excel", ".xlsx");
        try (FileOutputStream out = new FileOutputStream(filePath)) {
            workbook.write(out);
        } catch (IOException e) {
            return ResponseDto.<String>builder()
                    .code(-2)
                    .message("Error with " + e.getMessage())
                    .data("Excel file is not created")
                    .success(false)
                    .build();
        }

        return ResponseDto.<String>builder()
                .code(0)
                .message("OK")
                .data("Excel file is created")
                .success(true)
                .build();
    }

    @Override
    public ResponseDto<String> pdfGeneration() {
        Document document = new Document();
        String filePath = generateFilePath("upload_pdf", ".pdf");

        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            PdfWriter.getInstance(document, fos);
            document.open();

            PdfPTable table = new PdfPTable(6);
            addTableHeader(table);

            List<ProductEntity> productList = productRepository.findAll();
            for (ProductEntity product : productList) {
                addRow(table, product);
            }

            document.add(table);
            document.close();
        } catch (DocumentException | IOException e) {
            return ResponseDto.<String>builder()
                    .code(-2)
                    .message("Error with " + e.getMessage())
                    .data("Pdf file is not created")
                    .success(false)
                    .build();
        }

        return ResponseDto.<String>builder()
                .code(0)
                .message("OK")
                .data("Pdf file is created")
                .success(true)
                .build();
    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("Id", "Name", "Color", "Size", "Price", "CreatedAt")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.GREEN);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRow(PdfPTable table, ProductEntity product) {
        table.addCell(String.valueOf(product.getId()));
        table.addCell(product.getName());
        table.addCell(product.getColor());
        table.addCell(String.valueOf(product.getWeight()));
        table.addCell(String.valueOf(product.getPrice()));
        table.addCell(product.getCreatedAt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    private String generateFilePath(String folder, String ext) {

        LocalDate localDate = LocalDate.now();
        String path = localDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        File file = new File(folder + "/" + path);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uuid = UUID.randomUUID().toString();
        return file.getPath() + "/" + System.currentTimeMillis() + uuid + "." + ext;

    }


}
