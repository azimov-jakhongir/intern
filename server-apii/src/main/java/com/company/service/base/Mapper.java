package com.company.service.base;

import com.company.model.dto.ResponseDto;
import com.company.model.dto.UserDto;
import com.company.model.entity.UserEntity;
import com.company.model.form.UserForm;
import org.springframework.stereotype.Service;

@Service
public class Mapper {

    public UserDto formToDto(UserForm userForm, Long id) {
        return UserDto.builder()
                .id(id)
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .username(userForm.getUsername())
                .password(userForm.getPassword())
                .role(String.valueOf(userForm.getRole()))
                .build();
    }

    public UserDto entityToDto(UserEntity user, Long index) {
        return UserDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .username(user.getUsername())
                .password(user.getPassword())
                .role(user.getRoles().toString())
                .index(index)
                .build();
    }

    public ResponseDto<?> convertResponseDto(Object data, String message, Boolean success, int code) {


        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setSuccess(success);
        responseDto.setData(data);
        responseDto.setCode(code);
        responseDto.setMessage(message);
        return responseDto;
    }


}
