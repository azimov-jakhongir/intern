package com.company.service;

import com.company.model.dto.ResponseDto;
import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {
    ResponseDto<String> exelCreate() throws IOException;

    ResponseDto<String> pdfGeneration() throws DocumentException, FileNotFoundException;
}
