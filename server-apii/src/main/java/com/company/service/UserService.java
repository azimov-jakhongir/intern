package com.company.service;

import com.company.model.form.UserForm;
import com.company.model.form.UserLoginForm;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<?> doRegister(UserForm userForm);

    ResponseEntity<?> doLogin(UserLoginForm userLoginForm);

    ResponseEntity<?> addUser(UserForm userForm);
}
