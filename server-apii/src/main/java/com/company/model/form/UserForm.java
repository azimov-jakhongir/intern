package com.company.model.form;

import com.company.model.enums.Roles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserForm {

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private Roles role;

}
