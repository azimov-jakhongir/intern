package com.company.model.dto;

import lombok.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaginationInfo {
    private int pageNumber;
    private int pageSize;
    private int totalPages;
    private long totalElements;

}
