package com.company.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Component
public class ResponseDto<T> {
    private Boolean success;

    private Integer code;

    private String message;

    private T data;
//    private PaginationInfo paginationInfo;

//    @Transactional(readOnly = true)
//    public void setPaginationInfo(PaginationInfo paginationInfo) {
//        this.paginationInfo = paginationInfo;
//    }
}
