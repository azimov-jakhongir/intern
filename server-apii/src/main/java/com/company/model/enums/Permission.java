package com.company.model.enums;


public enum Permission {

    READ("READ"),
    CREATE("CREATE"),
    UPDATE("UPDATE"),
    DELETE("DELETE");

    private final String name;


    Permission(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
