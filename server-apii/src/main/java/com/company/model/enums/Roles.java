package com.company.model.enums;

import java.util.ArrayList;
import java.util.List;

import static com.company.model.enums.Permission.*;


public enum Roles {

    SUPER_ADMIN(List.of(Permission.READ, Permission.CREATE, Permission.UPDATE, Permission.DELETE)),
    ADMIN(List.of(Permission.READ, Permission.CREATE, Permission.UPDATE)),
    USER(List.of(Permission.READ)),
    MODERATOR(List.of(Permission.READ, Permission.CREATE));

    final List<Permission> permissionList;

    Roles(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }
    public List<String> getPermissions(){
        List<String> list = new ArrayList<>(this.permissionList.stream()
                .map(Permission::getName)
                .toList());
        list.add("ROLE_" + this.name());
        return list;
    }


}
