package com.company.controller;

import com.company.model.entity.ProductEntity;
import com.company.model.form.ProductForm;
import com.company.service.ProductService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
@AllArgsConstructor
@Slf4j
@SecurityRequirement(name = "Authorization")
public class ProductController {
    private final ProductService productService;

    @PostMapping("/create")
    public ResponseEntity<?> createProduct(@RequestBody ProductForm productForm) {
        log.info("Creating product with product form {}", productForm);
        return productService.createProduct(productForm);
    }

    @PreAuthorize("hasAnyAuthority('UPDATE')")
    @PutMapping("/update/{productId}")
    public ResponseEntity<?> updateProduct(
            @PathVariable("productId") Long productId,
            @RequestBody ProductForm productForm) {
        return productService.updateByProductId(productId, productForm);
    }

    @PreAuthorize("hasAnyAuthority('READ')")
    @GetMapping("/get/{productId}")
    public ResponseEntity<?> getProduct(
            @PathVariable("productId") Long productId
    ) {
        log.info("Find by product id {}", productId);
        return productService.findByProductId(productId);
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllProducts(){
        return productService.findAllProducts();
    }

    @PreAuthorize("hasAnyAuthority('DELETE')")
    @DeleteMapping("/delete/{productId}")
    public ResponseEntity<?> deleteProduct(@PathVariable("productId") Long productId) {
        return productService.deleteByProductId(productId);
    }

}
