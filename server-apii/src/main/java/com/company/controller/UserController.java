package com.company.controller;

import com.company.model.form.UserForm;
import com.company.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
@Slf4j
@SecurityRequirement(name = "Authorization")
public class UserController {

    /*
    * TODO
    *  add user
    * get user by id
    * get users by list
    * update by user id
    * delte by user id
    *
    * */
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAnyAuthority('SUPER_ADMIN')")
    @PostMapping("/add-user")
    public ResponseEntity<?> addUser(@RequestBody UserForm userForm) {
        log.info("Received create user request with OrgForm: {}", userForm);
        return userService.addUser(userForm);
    }



}
