package com.company.controller;

import com.company.model.dto.ResponseDto;
import com.company.service.FileService;
import com.itextpdf.text.DocumentException;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;

@RestController
@RequestMapping("/api/file")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "Authorization")
public class FileController {
    private final FileService fileService;
    @GetMapping("excel")
    @PreAuthorize("hasAnyAuthority('UPDATE')")
    public ResponseDto<String> exelGeneration() throws IOException {
        return fileService.exelCreate();
    }
    @GetMapping("pdf")
    @PreAuthorize("hasAnyAuthority('UPDATE')")
    public ResponseDto<String> pdfGeneration() throws DocumentException, FileNotFoundException {
        return fileService.pdfGeneration();
    }



}
