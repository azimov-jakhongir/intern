package com.company.controller;

import com.company.model.form.UserForm;
import com.company.model.form.UserLoginForm;
import com.company.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@Slf4j
@SecurityRequirement(name = "Authorization")
public class AuthController {
    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }
    @PostMapping("/register")
    public ResponseEntity<?> doRegister(@RequestBody UserForm userForm) {
        log.info("Received create user request with OrgForm: {}", userForm);
        return userService.doRegister(userForm);
    }

    @PostMapping("/login")
    public ResponseEntity<?> doLogin(@RequestBody UserLoginForm userLoginForm) {
        log.info("Received login user request with OrgForm: {}", userLoginForm);
        return userService.doLogin(userLoginForm);
    }


}
