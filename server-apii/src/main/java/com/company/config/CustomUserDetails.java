package com.company.config;

import com.company.model.entity.UserEntity;
import com.company.model.enums.Roles;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class CustomUserDetails implements UserDetails {


    private String username;
    private String password;
    private String role;

    private final List<GrantedAuthority> authorities;

    public CustomUserDetails(UserEntity entity) {
        System.out.println("entityfytdhjyhgj");
        username = entity.getUsername();
        password = entity.getPassword();
        role = (entity.getRoles().toString());
        authorities = null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Roles.valueOf(role)
                .getPermissions().stream()
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
